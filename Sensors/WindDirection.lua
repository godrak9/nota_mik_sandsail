local sensorInfo = {
	name = "Wind",
	desc = "Return actual heading and strength of wind",
	author = "PavelMikus",
	date = "2019-04-14",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT,
		fields = {"x","z", "strength"}
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return current wind statistics
return function()
	local dirX, dirY, dirZ, strength, normDirX, normDirY, normDirZ = SpringGetWind()
	return {
		x = normDirX,
		z = normDirZ,
		strength = strength
	}
end
